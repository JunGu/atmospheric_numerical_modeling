    PROGRAM main
      implicit none
      real(kind = 8), parameter :: g = 9.8, epsilon_p = 1.e-16, karman = 0.35
      real(kind = 8):: z1,u1,t1,z2,u2,t2 ! Read from keyboard
      real(kind = 8) :: dz, Rib, arf, dzL_t, dzL_t1
      real(kind = 8) :: eqm, eqh, Lmo, Pesim, Pesih 
      real(kind = 8) :: ustar, tstar
      integer(kind = 4) :: nn


      !write(*,*) "Input z1, u1, t1  in turn"
      !!read(*,"(3x(F6.3))") z1,u1,t1
      !read(*,*) z1,u1,t1
      !write(*,*) "Input z2, u2, t2  in turn"
      !read(*,*) z2,u2,t2
      z1 = 5. ; z2 = 10.
      u1 = 0.156 ; u2 = 1.552
      t1 = 11.21 ; t2 = 11.18

      dz = z2 - z1 
      Rib = g*dz*(t2-t1)*2/((t2+t1)*(u2-u1)**2)
      write(*,*) Rib
      arf = log(z2/z1)
      write(*,*) arf
      dzL_t = Rib*arf/0.74
      dzL_t1 = dzL_t+10000

      do nn = 0, 1100
        if(abs(dzL_t - dzL_t1) < epsilon_p .or. nn > 1000)then
            exit
        else 
            dzL_t1 = dzL_t
            Lmo = dz/dzL_t
            write(*,*) "dz:",dz
            write(*,*) "dzL_t: ",dzL_t
            write(*,*) "Lmo",Lmo
            if(abs(Lmo) < 1.e-20) then
                exit
            else
                eqm = arf - Pesim(z2/Lmo) + Pesim(z1/Lmo)
                eqh = arf - Pesih(z2/Lmo) + Pesih(z1/Lmo)
                write(*,*) "eqh",eqh
                if(abs(eqh) < 1.e-20) then
                    exit
                end if
                dzL_t  = Rib*eqm**2/(eqh*0.74)
                write(*,*) "dzL_t:",dzL_t
                write(*,*) "dzL_t -dzL_t1",abs(dzL_t - dzL_t1)
            end if
            write(*,*) nn 
        end if
      end do 
      Lmo = dz/dzL_t
      !Lmo = -2032.84
      write(*,*) "Lmo",Lmo
      ustar = karman*(u2-u1)/(arf - Pesim(z2/Lmo) + Pesim(z1/Lmo) )
      tstar = karman*(t2-t1)/(0.74*(arf - Pesih(z2/Lmo) + Pesih(z1/Lmo)))

      write(*,"(A4,F6.3,2X,A4,F6.3,2X,A4,F6.3)") "z1= ",z1,"u1= ",u1,"t1= ",t1
      write(*,"(A4,F6.3,2X,A4,F6.3,2X,A4,F6.3)") "z2= ",z2,"u2= ",u2,"t2= ",t2
      write(*,"(A34)") "For given data, output as follows:"
      write(*,"(A7,F10.7,2X,A7,F10.7,2X,A7,F8.2,2X)") "usatr= ",ustar,"tstar= ",tstar,"Lmo= ",Lmo
    END PROGRAM
    REAL(kind = 8) FUNCTION Pesim(RzL)
        implicit none
        real(kind = 8) ::RzL
        !Local Variables
        real(kind = 8) ::Am, x
        real(kind = 8) ::aa, bb
        if(Rzl < 0) then
            Am = 15
            x  = (1-Am*RzL)**0.25
            Pesim = log((1+x**2)/2) + 2*log((1+x)/2) - 2*atan(x) + 1.57
        else
            aa = 6.1
            bb = 2.5
            Pesim =  -aa * log(RzL + (1 + RzL**bb)**(1/bb))
        end if
        return 
    END FUNCTION Pesim

    REAL(kind = 8) FUNCTION Pesih(RzL)
        implicit none
        real(kind = 8) ::RzL
        !Local Variables
        real(kind = 8) ::Ah, y
        real(kind = 8) ::cc, dd
        if(RzL < 0) then
            Ah = 9 
            y  = (1-Ah*RzL)**0.5
            Pesih = 2*log(1.0/2 + y/2)
        else 
            cc = 5.3
            dd = 1.1
            Pesih = -cc * log(RzL + (1 + RzL**dd)**(1/dd))
        end if 
        return
    END FUNCTION Pesih


