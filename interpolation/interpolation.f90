        program interpolation
                implicit none
                real data(5,177),lon_sta(177),lat_sta(177),hgt_sta(177),speed_sta(177),dir_sta(177),dir_sta_d2r(177)
                real u_sta(177),v_sta(177)
                real,parameter :: PI = 3.14159267
                real lon_grid(21),lat_grid(16),hgt(21,16),u(21,16),v(21,16)
                real w,sum_fw,fw,sum_w
                real uw,sum_uw,vw,sum_vw
                integer i,j,k 
                real google_distance

                open(10,file="data.txt")
                read(10,*)((data(i,j), i=1, 5), j=1, 177)
                lon_sta = data(1,:)
                lat_sta = data(2,:)
                hgt_sta = data(3,:)
                speed_sta = data(4,:)
                dir_sta = data(5,:)
                close(10)
                dir_sta_d2r = dir_sta * PI / 180.
                u_sta = speed_sta * sin(dir_sta_d2r)
                v_sta = speed_sta * cos(dir_sta_d2r)
                do j=1, 21
                    lon_grid(j) = 100+(j-1)*1.
                    do i=1, 16
                        lat_grid(i) = 25+(i-1)*1.
                        sum_fw = 0.
                        sum_uw = 0.
                        sum_vw = 0.
                        sum_w = 0.
                        do k=1, 177
                            if(hgt_sta(k).ne.-999) then !filter missing value
                                w = 1./(0.001+(lon_sta(k)-lon_grid(j))**2+ &
                                     (lat_sta(k)-lat_grid(i))**2)
                                fw = hgt_sta(k)*w 
                                uw = u_sta(k)*w
                                vw = v_sta(k)*w
                                sum_fw = sum_fw + fw
                                sum_uw = sum_uw + uw
                                sum_vw = sum_vw + vw
                                sum_w = sum_w + w
                            end if
                        end do
                        if(sum_w.ne.0) then
                            hgt(j,i) = sum_fw/sum_w
                            u(j,i) = sum_uw/sum_w
                            v(j,i) = sum_vw/sum_w
                        else
                            hgt(j,i) = -999
                            u(j,i) = -999
                            v(j,i) = -999

                        end if
                    end do
                end do
                call write_nc(hgt,u,v)
        end program interpolation

        real function google_distance(lon1,lat1,lon2,lat2)
                implicit none
                real,parameter :: RE=6378.137
                real,parameter:: Radian=1.745329E-2
                real lon1,lat1,lon2,lat2
                real a1,b1,a2,b2,cs,a,b
!               radian=4*atan(1.0)/180.0
                a1=lon1*radian
                a2=lon2*radian
                b1=lat1*radian
                b2=lat2*radian
                a =b1-b2
                b =a1-a2
                cs =2*asin(sqrt(sin(a/2)**2+cos(b1)*cos(b2)*sin(b/2)**2))
                google_distance =(RE*cs)**2! square
                return 
        end function google_distance
        
        subroutine write_nc(hgt,u,v)
                implicit none
                include '/usr/include/netcdf.inc'
                character(len=256) ::newfile
                integer,parameter :: nlat = 16,nlon = 21
                integer ::statu,ncid,nlatid,nlonid,hgtid,uid,vid,latid,lonid,&
                          i,j
                integer ::hgtdims(2),udims(2),vdims(2),latdims(1),lonims(1)
                real    ::hgt(nlon,nlat),u(nlon,nlat),v(nlon,nlat),lat(nlat),lon(nlon)
                do i=1,16
                       lat(i)=25+(i-1)*1.
                end do
                do j=1,21
                       lon(j)=100+(j-1)*1.
                end do 
                
                newfile = "interpolated_hgt.nc"
                write(*,*) "Creat New NETcdf", trim(newfile)
                statu=nf_create(trim(newfile),nf_noclobber,ncid)
                if(statu /= nf_noerr) call handle_err(statu)

                write(*,*) "Define Dimensions"
                statu=nf_def_dim(ncid,'lon',nlon,nlonid)
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_def_dim(ncid,'lat',nlat,nlatid)
                if(statu /= nf_noerr) call handle_err(statu)

                write(*,*)"Define Variables"
                hgtdims(1)=nlonid
                hgtdims(2)=nlatid
                statu=nf_def_var(ncid,"hgt",nf_float,2,hgtdims,hgtid)
                if(statu /= nf_noerr) call handle_err(statu)

                udims(1)=nlonid
                udims(2)=nlatid
                statu=nf_def_var(ncid,"u",nf_float,2,udims,uid)
                if(statu /= nf_noerr) call handle_err(statu)

                vdims(1)=nlonid
                vdims(2)=nlatid
                statu=nf_def_var(ncid,"v",nf_float,2,vdims,vid)
                if(statu /= nf_noerr) call handle_err(statu)

                statu=nf_def_var(ncid,"lon",nf_float,1,nlonid,lonid)
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_def_var(ncid,"lat",nf_float,1,nlatid,latid)
                if(statu /= nf_noerr) call handle_err(statu)

                write(*,*)"Put Attributes"
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,hgtid,"description",19,&
                "Geopotential Height")
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,hgtid,"units",1,"m")
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,hgtid,"coordinates",11,&
                "XLONG XLAT")

                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,uid,"description",11,&
                "uZonal wind")
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,uid,"units",3,"m/s")
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,uid,"coordinates",11,&
                "XLONG XLAT")

                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,vid,"description",16,&
                "uMeridional wind")
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,vid,"units",3,"m/s")
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,vid,"coordinates",11,&
                "XLONG XLAT")

                statu=nf_put_att_text(ncid,latid,"units",12,"degree_north")
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,latid,"description",27,&
                "LATITUDE, SOUTH IS NEGATIVE")
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,latid,"axis",1,"X")
                if(statu /= nf_noerr) call handle_err(statu)

                statu=nf_put_att_text(ncid,lonid,"units",11,"degree_east")
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,lonid,"description",27,&
                "LONGITUDE, WEST IS NEGATIVE")
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_att_text(ncid,lonid,"axis",1,"y")
                if(statu /= nf_noerr) call handle_err(statu)


                write(*,*)"End Defination"
                statu=nf_enddef(ncid)
                if(statu /= nf_noerr) call handle_err(statu)

                write(*,*)"Put Variable"
                statu=nf_put_var_real(ncid,hgtid,hgt)
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_var_real(ncid,uid,u)
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_var_real(ncid,vid,v)
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_var_real(ncid,lonid,lon)
                if(statu /= nf_noerr) call handle_err(statu)
                statu=nf_put_var_real(ncid,latid,lat)
                if(statu /= nf_noerr) call handle_err(statu)

                write(*,*)"Close nc"
                statu=nf_close(ncid)
                if(statu /= nf_noerr) call handle_err(statu)
        end

        subroutine handle_err(statu)
                implicit none
                integer statu
                write(*,*)"error!"
                stop
        end
