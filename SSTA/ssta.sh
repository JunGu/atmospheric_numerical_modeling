###################################################
# File Name: ssta.sh
# Author: JunGu
# Mail: gj99@mail.ustc.edu.cn
# Created Time: Sat 10 Oct 2020 03:19:28 PM CST
####################################################
#!/bin/bash
cdo selname,sst sst98to18.nc sstnew.nc
cdo sellonlatbox,100,280,-30,30 sstnew.nc sstbox.nc
cdo ymonmean sstbox.nc sstbox_ymonmean.nc
cdo sub sstbox.nc sstbox_ymonmean.nc result.nc
rm sstnew.nc; rm sstbox.nc; rm sstbox_ymonmean.nc
