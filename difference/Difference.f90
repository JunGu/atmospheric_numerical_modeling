    Program Difference
        implicit none
        !include '/usr/include/netcdf.inc'
        real(kind=8) ::Z(12,8), U(12,8), V(12,8), lat(8), lon(12)
        real(kind=8) ::Ug(12,8), Vg(12,8), Div(12,8), Vor(12,8), Vor_g(12,8)
        real(kind=8) ::Z1(8,12), Ug1(8,12), Vg1(8,12)
        character(len=256) ::filename, outname
        integer(kind=4):: i, j
        real(kind=8) :: dx, dy, lat_r, f
        real(kind=8), parameter :: g=9.8, omega=7.292e-5, Re=6.371e6, Pi=3.1415926
        real(kind=8), parameter :: radian=Pi/180

        filename = "Zuv.dat"
        open(12,file=trim(filename),form="formatted",status="old")
        !read(12,*) ((Z(i,j), i=1, 12), j=1, 8)
        !read(12,*) ((U(i,j), i=1, 12), j=1, 8)
        !read(12,*) ((V(i,j), i=1, 12), j=1, 8)
        read(12,*) ((Z(i,j), j=1, 8), i=1, 12)
        read(12,*) ((U(i,j), j=1, 8), i=1, 12)
        read(12,*) ((V(i,j), j=1, 8), i=1, 12)
        close(12)
        !read(12,"(E15.6)") ((Z(i,j), i=1, 12), j=1, 8)
        !read(12,"(E15.6)") ((U(i,j), i=1, 12), j=1, 8)
        !read(12,"(E15.6)") ((V(i,j), i=1, 12), j=1, 8)
        write(*,*) Z(1,:)

        do j=1,8
            lat(j) = 10 + (j-1)*5
        end do
        do i=1,12
            lon(i) = 110 + (i-1)*5
        end do

        dy = Re*5*radian !Convert Deg to Rad for math function sin&cos
                         !5 refer to resolution of data
        Ug = -999. !Fill_Value
        Vg = -999. !Fill_Value

        do j=2,7
            lat_r = lat(j) * radian
            f = 2*omega*sin(lat_r)
            dx = dy*cos(lat_r)
            do i=2,11
                Ug(i,j)=-g*(Z(i,j+1)-Z(i,j-1))/(f*2*dy)
                Vg(i,j)= g*(Z(i+1,j)-Z(i-1,j))/(f*2*dx)
            end do
        end do
        !boundary difference
        j=1
        lat_r = lat(j)*radian
        f = 2*omega*sin(lat_r)
        dx = dy*cos(lat_r)
        do i=2,11
            Ug(i,j)=-g*(Z(i,j+1)-Z(i,j))/(f*dy)
            Vg(i,j)= g*(Z(i+1,j)-Z(i-1,j))/(f*2*dx)
        end do
            
        j=8
        lat_r = lat(j)*radian
        f = 2*omega*sin(lat_r)
        dx = dy*cos(lat_r)
        do i=2,11
            Ug(i,j)=-g*(Z(i,j)-Z(i,j-1))/(f*dy)
            Vg(i,j)= g*(Z(i+1,j)-Z(i-1,j))/(f*2*dx)
        end do

        i=1
        do j=2,7
            lat_r = lat(j)*radian
            f = 2*omega*sin(lat_r)
            dx = dy*cos(lat_r)
            Ug(i,j)=-g*(Z(i,j+1)-Z(i,j-1))/(f*2*dy)
            Vg(i,j)= g*(Z(i+1,j)-Z(i,j))/(f*dx)
        end do
        
        i=12
        do j=2,7
            lat_r = lat(j)*radian
            f = 2*omega*sin(lat_r)
            dx = dy*cos(lat_r)
            Ug(i,j)=-g*(Z(i,j+1)-Z(i,j-1))/(f*2*dy)
            Vg(i,j)= g*(Z(i,j)-Z(i-1,j))/(f*dx)
        end do

        !Corner difference
        j=1
        lat_r = lat(j)*radian
        f = 2*omega*sin(lat_r)
        dx = dy*cos(lat_r)
        Ug(1,1)=-g*(Z(1,2)-Z(1,1))/(f*dy)
        Vg(1,1)= g*(Z(2,1)-Z(1,1))/(f*dx)
        Ug(12,1)=-g*(Z(12,2)-Z(12,1))/(f*dy)
        Vg(12,1)= g*(Z(12,1)-Z(11,1))/(f*dx)

        j=8
        lat_r = lat(j)*radian
        f = 2*omega*sin(lat_r)
        dx = dy*cos(lat_r)
        Ug(1,8)=-g*(Z(1,8)-Z(1,7))/(f*dy)
        Vg(1,8)= g*(Z(2,8)-Z(1,8))/(f*dx)
        Ug(12,8)=-g*(Z(12,8)-Z(12,7))/(f*dy)
        Vg(12,8)= g*(Z(12,8)-Z(11,8))/(f*dx)

        outname = "UgVg.dat"
        open(14,file=trim(outname),form="formatted",status="replace")
        write(14,"(8(F13.6,2X))") transpose(Z)
        write(14,"(8(F13.6,2X))") transpose(U)
        write(14,"(8(F13.6,2X))") transpose(V)
        write(14,"(8(F13.6,2X))") transpose(Ug)
        write(14,"(8(F13.6,2X))") transpose(Vg)
        close(14)

    END PROGRAM 
