load "$my_config/nclscripts/my_code.ncl"
begin
    start_time = get_cpu_time()
    namelists = systemfunc("ls -tr ./output/time_step=900/*.nc")
    number_nc = dimsizes(namelists)
    ;print(namelists)
    system("mkdir -p ./pics/time_step=900")

;prepare for plot
    cmap   = read_colormap_file("BlAqGrYeOrReVi200")
    pltype = "png"
;Set Resources
    res = True
    res@gsnDraw  = False
    res@gsnFrame = False
    res@gsnMaximize = True
    res@gsnAddCyclic= False

    res@tmXTOn = False
    res@tmYROn = False
    res@tmXBLabelFontHeightF = 0.012
    res@tmYLLabelFontHeightF = 0.012
    res@vpHeightF = 0.6
    res@vpWidthF  = 0.8
    res@gsnLeftString  = ""
    res@gsnRightString = ""
    ;;set map
    mpres = res
    delete(mpres@gsnAddCyclic)
    mpres@mpProjection          = "CylindricalEquidistant"
    mpres@pmTickMarkDisplayMode = "Always"
    mpres@mpLimitMode       = "LatLon"
    mpres@mpMinLatF         = 0.
    mpres@mpMaxLatF         = 55.
    mpres@mpMinLonF         = 90.
    mpres@mpMaxLonF         = 165.
    
    mpres@mpDataSetName            = "Earth..4"
    mpres@mpDataBaseVersion        = "MediumRes"
    mpres@mpOutlineOn              =  True
    ;res@mpOutlineSpecifiers      = (/"China:States","Taiwan"/)
    mpres@mpOutlineBoundarySets    = "AllBoundaries"
    
    mpres@mpGeophysicalLineColor      = "black"
    mpres@mpNationalLineThicknessF    = 4.0      ; for better looking images
    mpres@mpGeophysicalLineThicknessF = 3.0
    mpres@mpProvincialLineThicknessF  = 3.0
    
    mpres@mpGridAndLimbOn       = True
    mpres@mpGridSpacingF        = 5.0
    mpres@mpGridLineDashPattern = 2
    mpres@mpGridLineThicknessF  = 2.0

    ;;set contour
    cnres   = res
    cnres@cnFillOn             = True
    cnres@cnFillMode           = "RasterFill"
    cnres@cnRasterSmoothingOn  = True
    cnres@cnLinesOn            = False
    
    cnres@cnLevelSelectionMode = "ManualLevels"
    cnres@cnMinLevelValF       = 5100
    cnres@cnMaxLevelValF       = 5900
    cnres@cnLevelSpacingF      = 40.
    cnres@cnFillPalette        = cmap(:,:)
    
    cnres@pmLabelBarDisplayMode = "Always"
    ;cnres@lbTitleString         = "[gpm]"
    
    cnres@lbTitlePosition       = "Bottom"
    cnres@lbTitleDirection      = "Across"
    cnres@lbTitleAngleF         = 0.
    cnres@lbTitleFontHeightF    = 0.01
    cnres@lbTitleExtentF        = 0.03
    cnres@lbTitleOffsetF        = 0.01
    
    cnres@lbOrientation         = "Horizontal"
    cnres@lbBoxEndCapStyle      = "TriangleBothEnds"
    cnres@lbBoxLinesOn          = False
    cnres@lbBoxSeparatorLinesOn = False
    cnres@lbLabelFontAspectF    = 1.3125
    cnres@lbLabelFontHeightF    = 0.01
    cnres@pmLabelBarHeightF     = 0.08
   
    ;;set vector
    vcres                       = res
    vcres@vcGlyphStyle          = "LineArrow"
    vcres@vcRefLengthF          = 0.045
    vcres@vcLineArrowColor      = "Blue"
    vcres@vcMinDistanceF        = 0.03          ; set to small value to thin the vectors.
    vcres@vcLineArrowThicknessF = 3.            ; make the vectors thicker.
    
    vcres@vcRefAnnoOn               = True
    vcres@vcRefMagnitudeF           = 15.0
    vcres@vcRefAnnoString1          = "15"
    vcres@vcRefLengthF              = 0.03   ; define length of vec ref
    vcres@vcRefAnnoFontHeightF      = 0.01
    vcres@vcRefAnnoFontThicknessF   = 1.1
    vcres@vcRefAnnoSide             = "Top"
    vcres@vcRefAnnoString2On        = False
    vcres@vcRefAnnoPerimOn          = True
    vcres@vcRefAnnoOrthogonalPosF   = -0.11
    vcres@vcRefAnnoBackgroundColor  = "white"
    
    vcres@gsnRightString             = "Wind[m/s]"
    vcres@gsnRightStringFontHeightF  = 0.018
    do i=0,number_nc-1
        name = namelists(i)
        ;print(name)
        f = addfile(name,"r");;;
        U   = f->u
        V   = f->v
        hgt = f->z
        printMinMax(hgt,1)
        ;str_array=str_split(name,"/")
        ;str_array2=str_split(str_array(8),".")
        ;nc_name = str_array2(0)
        name_str="zuv_" + sprinti("%0.3i",i)
        ;print(name_str)
        wks =gsn_open_wks(pltype,"./pics/time_step=900/"+name_str)
        ;;creat plots
        ;;plot
        mpres@tiMainString = "At Forecast_time= "+sprintf("%5.2f",0.25*i)+" h"
        mpres@tiMainFontHeightF = 0.019
        map     = gsn_csm_map(wks,mpres)
        contour = gsn_csm_contour(wks,hgt,cnres)
        vector  = gsn_csm_vector(wks,U,V,vcres)
        ;;overlay
        overlay(map,contour)
        overlay(map,vector)
        draw(map)
        frame(wks)
    end do
end
