;---------------------------------------------------
; File Name: calculate.ncl
; Author: JunGu
; Mail: gj99@mail.ustc.edu.cn
; Created Time: Mon 23 Nov 2020 11:18:02 AM CST
;---------------------------------------------------
load "$my_config/nclscripts/my_code.ncl"
begin
    start_time = get_cpu_time()
    filename = "./TOA5_CR3000_ts_data_20151201_2015_12_25_1200.dat"
    nrows    = numAsciiRow(filename)
    ncols    = numAsciiCol(filename)
    print(nrows)
    print(ncols)
    ;data = readAsciiTable(filename,14,"float",2)
    ;printVarSummary(data)
    ;Opt = True
    ;Opt@fout = "test.txt"
    ;write_matrix(data,"14f9.2",Opt)
    strs  = asciiread(filename,-1,"string")
    delim =","
    nfileds = str_fields_count(strs(0),delim)

    u_str= str_get_field(strs, 3, delim)
    u    = stringtofloat(u_str(2:))
    u@_FillValue = -999.
    ;print(u(0:10))
    ntime = dimsizes(u)

    v_str= str_get_field(strs, 4, delim)
    v    = stringtofloat(v_str(2:))
    v@_FillValue = -999.

    w_str= str_get_field(strs, 5, delim)
    w    = stringtofloat(w_str(2:))
    w@_FillValue = -999.

    t_str= str_get_field(strs, 6, delim)
    t    = stringtofloat(t_str(2:))
    print(num(.not.ismissing(t)))
    printVarSummary(t)
    t@_FillValue = -999.

    ;Calculate Temperature structure function
    D_t    = new(1200,float); First 20 min
    diff_T = new(36000,float,"-999.")
    do i = 0, 1200-1
        do j = 0, 36000-i-1
            diff_T(j) = (t(j) - t(j+i))^2 ;for different lag
        end do
        D_t(i) = dim_avg(diff_T)
    end do
    printVarSummary(D_t)
    asciiwrite("Dt.txt",D_t)

    print_elapsed_time(start_time,get_cpu_time(),get_script_prefix_name())
end 
